%.html: %.rst
	rst2html $< > $@

html: policy.html

install: html
	scp policy.* alioth.debian.org:/home/groups/python-modules/htdocs

clean:
	rm -f *.html
